## **Jawaban Soal Pertama: SQL Test**

**Nama : Muhamad Iqbal Maulana** <br>
**Telp : 085791340248** <br>
**Posisi : Data Engineer** <br>
**Linkedin : https://www.linkedin.com/in/miqbalm/** <br>
**Email : iqbalmaulana.im270@gmail.com**


Soal URL : [Klik Disini](https://gitlab.com/jdsdataengineer/jds-dataengineer-oprec-test-case/-/blob/master/README.md?utm_campaign=oprec-dec-2021&utm_medium=googleform&utm_source=oprec-dec-2021#soal-pertama-sql-test)

**Screenshot**
![enter image description here](https://gitlab.com/pribadi1395821/jabar-digital-service-iqbal/de_sql_1/de_sql_1/-/raw/main/image/soal1-1.png)
**Gambar 1.1** Query kasus Covid-19 tanggal 2020-07-01 berdasarkan kepadatan jalan tanggal 2020-06-25 di jabar.

<br>

**Code**
Berikut ini Penulisan Query (Jalankan Menggunakan PSQL)

        WITH kepadatan_kendaraan AS (
        	SELECT
        		REPLACE(kemendagri_kabupaten_kode, '.', '') AS kemendagri_kabupaten_kode,
        		med_level,
        		avg_delay,
        		avg_speed_kmh,
        		med_total_records
        	FROM
        		aggregate_jams_cities_june_july_2020
        	WHERE
        		date = '2020-06-25'
        )
    SELECT
    	kc.tanggal AS date,
    	kc.nama_kab_kota AS nama_kab_kota,
    	kc.kode_kab_kota AS kode_kab_kota,
    	kc.konfirmasi_total,
    	kc.konfirmasi_sembuh,
    	kc.konfirmasi_meninggal,
    	kc.konfirmasi_aktif,
    	kc.konfirmasi_total_daily_growth,
    	ROUND(CAST(st.med_level AS numeric), 1) AS med_level,
    	st.avg_delay,
    	st.avg_speed_kmh,
    	ROUND(CAST(st.med_total_records AS numeric), 1) AS total_records
    FROM
    	kasus_konfirmasi_juli kc
    LEFT JOIN
    	kepadatan_kendaraan st
    ON
    	kc.kode_kab_kota = st.kemendagri_kabupaten_kode
    WHERE
    	kc.tanggal = '2020-07-01'
    ORDER BY
    	kc.tanggal,
    	kc.kode_kab_kota;
<br>

**Hasil Eksekusi Query**
![enter image description here](https://gitlab.com/pribadi1395821/jabar-digital-service-iqbal/de_sql_1/de_sql_1/-/raw/main/image/soal1-2.png)
**Gambar 1.2** Hasil Eksekusi Query kasus Covid-19 tanggal 2020-07-01 berdasarkan kepadatan jalan tanggal 2020-06-25 di jabar.
Berdasarkan pada Gambar 1.2, menunjukkan data 27 kota dan kabupaten di jawa barat yang terkonfirmasi kasus Covid-19 pada tanggal 2020-07-01 yang di sandingkan dengan data agregat kepadatan jalan pada tanggal 2020-06-25. 
<br>

**Hasil Observasi**
 *Hubungan Kepadatan Kendaraan dan Kasus Covid-19*:
 - Kabupaten/kota dengan tingkat kemacetan tinggi pada tanggal 2020-06-25 mungkin menunjukkan peningkatan kasus pada tanggal 2020-07-01.
 - Kabupaten/kota dengan kecepatan kendaraan rendah (menunjukkan kemacetan tinggi) juga menunjukkan pola peningkatan kasus.
<br>

**Kesimpulan**
-   Tingkat aktivitas masyarakat yang tinggi, tercermin dari kemacetan lalu lintas, mungkin berkontribusi pada peningkatan penyebaran Covid-19.
-   Pemerintah dan otoritas kesehatan mungkin perlu mempertimbangkan pembatasan aktivitas masyarakat di daerah dengan tingkat kemacetan tinggi.

Data lalu lintas dapat digunakan sebagai indikator awal untuk memprediksi lonjakan kasus Covid-19 dan merencanakan intervensi yang diperlukan.
